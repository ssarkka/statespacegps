Source codes for reproducing the numerical experiments in S. Sarkka and R. Piche (2014), On convergence and accuracy of state-space approximations of squared exponential covariance functions.


# Authors #

Simo Särkkä <simo.sarkka@aalto.fi> (joint work with Robert Piche)

# Overview #

This repository contains the Matlab codes to reproduce the results in the paper

* S. Sarkka and R. Piche (2014). On convergence and accuracy of state-space approximations of squared exponential covariance functions. In Proceedings of MLSP. ([PDF](http://becs.aalto.fi/~ssarkka/pub/ss_se_mlsp.pdf))

The software is distributed under the GNU General Public Licence (version 2 or later); please refer to the file  Licence.txt, included with the software, for details.

# Instructions for use #

Please proceed as follows:

* Clone the git repository to some local directory. The Matlab codes can be found in the **matlab/** subfolder. If you wish not use the git, you can also download the repository contents via the **Download**-link on the left.

* Start Matlab and change the current directory to the **matlab/** subfolder in the repository.

* To generate Figure 1 in the paper above, issue the following command at Matlab prompt:
```
#!matlab
   conv_test1
```

* To generate Figures 2-3, issue the following command:
```
#!matlab
   gp_test1
```

After running the above commands, you should find the generated eps-files in the current directory (which should be the subfolder **matlab/**).

# Bug tracking and commenting #

To report a bug or formally suggest improvements, please use the **Issues**-link on the left. You can also find some more information and give free-form comments on the page behind the **Wiki**-link on the left.